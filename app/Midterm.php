<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Midterm extends Model
{
    
    protected $fillable = ['question_name','question'];
}
