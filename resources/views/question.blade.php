<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
    <div class="container">
    <table class="table table-striped">
    <thead>
      <tr>
        <th>ID</th>
        <th>Question_name</th>
        <th>Question</th>
      </tr>
    </thead>
    <tbody>
      @foreach($midterms as $post)
      <tr>
        <td>{{$post['id']}}</td>
        <td>{{$post['question_name']}}</td>
        <td>{{$post['question']}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>
  </div>
    </body>
</html>